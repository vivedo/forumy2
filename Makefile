
run:
	./gradlew run

package:
	./gradlew shadowJar

runpackage:
	./gradlew runShadow

devdb:
	docker-compose -f docker-compose.dev.yml up

clean:
	rm -rf out/ build/