package me.vivedo.forumy2

import io.ktor.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*


fun main(args: Array<String>){
    val server = embeddedServer(Netty, port = 8080, module = Application::main)
    server.start(wait = true)
}