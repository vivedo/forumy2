package me.vivedo.forumy2.controllers

import io.ktor.application.call
import io.ktor.response.*
import io.ktor.routing.*
import me.vivedo.forumy2.models.UserClass
import org.jetbrains.exposed.sql.transactions.transaction

class UserClassController {

    fun get() : List<UserClass> =
        transaction {
            UserClass.all().toList()
        }


    fun get(id : Int) : UserClass? =
        transaction {
            UserClass.findById(id)
        }

}

fun Route.userClassesRoute(){
    val ctrl = UserClassController()

    get{
        call.respond(ctrl.get())
    }

    get("{id}"){
        call.respond(ctrl.get(Integer.parseInt(call.parameters["id"]))!!) }
}