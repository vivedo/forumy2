package me.vivedo.forumy2.controllers

import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.*
import me.vivedo.forumy2.utils.APIResponseFactory

class IndexController(){}

fun Route.indexRoute() {
    get{
        call.respond(APIResponseFactory.generate(mapOf("message" to "Welcome to Forumy API")))
    }
}
