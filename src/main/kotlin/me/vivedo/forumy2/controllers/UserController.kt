package me.vivedo.forumy2.controllers

import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import me.vivedo.forumy2.models.User
import me.vivedo.forumy2.models.UserClass
import org.jetbrains.exposed.sql.transactions.transaction

class UserController {

    fun get() : List<User> =
            transaction {
                User.all().toList()
            }


    fun get(id : Int) : User? =
            transaction {
                User.findById(id)
            }
}

fun Route.usersRoute(){
    val ctrl = UserController()

    get{
        call.respond(ctrl.get())
    }

    get("{id}"){
        call.respond(ctrl.get(Integer.parseInt(call.parameters["id"]))!!) }
}