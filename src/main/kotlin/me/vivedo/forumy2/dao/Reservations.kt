package me.vivedo.forumy2.dao


import org.jetbrains.exposed.dao.IntIdTable

object Reservations : IntIdTable() {
    val user = reference("user", Users)
    val courseClass = reference("course_class", CourseClasses)
}