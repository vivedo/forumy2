package me.vivedo.forumy2.dao

import org.jetbrains.exposed.dao.IntIdTable

object Users : IntIdTable() {
    val name = varchar("name", length = 50)
    val surname = varchar("surname", length = 50)
    val userClass = reference("user_class", UserClasses)
}
