package me.vivedo.forumy2.dao

import org.jetbrains.exposed.dao.IntIdTable

object Locations : IntIdTable(){
    val displayname = varchar("displayname", 50)
}