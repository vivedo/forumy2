package me.vivedo.forumy2.dao

import org.jetbrains.exposed.dao.IntIdTable

object Courses : IntIdTable(){
    val displayname = varchar("displayname", length = 128)
    val description = text("description")
}
