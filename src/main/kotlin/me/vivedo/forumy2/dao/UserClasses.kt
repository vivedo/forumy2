package me.vivedo.forumy2.dao

import org.jetbrains.exposed.dao.IntIdTable

object UserClasses : IntIdTable() {
    val displayname = varchar("displayname", 50)
}