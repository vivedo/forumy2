package me.vivedo.forumy2.dao

import org.jetbrains.exposed.dao.IntIdTable

object CourseClasses : IntIdTable(){
    val course = reference("course", Courses)
    val partecipantsLimit = integer("partecipants_limit")
}