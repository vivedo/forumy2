package me.vivedo.forumy2

import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.SerializationFeature
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.response.*
import io.ktor.routing.*
import me.vivedo.forumy2.controllers.*
import me.vivedo.forumy2.dao.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.PrintWriter
import java.io.StringWriter
import kotlin.system.exitProcess

fun Application.main(){
    install(DefaultHeaders)
    install(CORS)
    install(CallLogging)
    install(ContentNegotiation) {
        jackson{
            disable(MapperFeature.AUTO_DETECT_CREATORS,
                    MapperFeature.AUTO_DETECT_FIELDS,
                    MapperFeature.AUTO_DETECT_GETTERS,
                    MapperFeature.AUTO_DETECT_IS_GETTERS)
            disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
        }
    }

    install(StatusPages) {
        status(HttpStatusCode.NotFound){
            call.respond(mapOf("error" to HttpStatusCode.NotFound.value, "message" to HttpStatusCode.NotFound.description))
        }
        status(HttpStatusCode.Forbidden){
            call.respond(mapOf("error" to HttpStatusCode.Forbidden.value, "message" to HttpStatusCode.Forbidden.description))
        }
        status(HttpStatusCode.BadRequest){
            call.respond(mapOf("error" to HttpStatusCode.BadRequest.value, "message" to HttpStatusCode.BadRequest.description))
        }

        exception<Throwable> { cause ->
            val sw = StringWriter()
            cause.printStackTrace(PrintWriter(sw))
            call.respondText(sw.toString())
        }
    }

    Database.connect("jdbc:postgresql://localhost/forumy2?user=forumy2&password=forumy2&ssl=false", "org.postgresql.Driver")

    // Generate DB structure
    transaction {
        create(Courses, Locations, CourseClasses, UserClasses, Users, Reservations)
    }

    // Setup Routing
    routing {
        route("/v1"){
            route("/")              { indexRoute()         }
            route("/user-classes")  { userClassesRoute()   }
            route("/users")         { usersRoute()         }
            //route("/locations")     { locationsRoute()     }
            //route("/course-classes"){  }
            //route("/courses")       { coursesRoute()       }
            //route("/reservations")  { reservationsRoute()  }
        }

        get("/stop"){
            exitProcess(0)
        }
    }


}
