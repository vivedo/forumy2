package me.vivedo.forumy2.utils

import com.fasterxml.jackson.annotation.JsonProperty

abstract class APIResponseFactory {
    companion object {
        fun generate(payload: Any, statusCode: Int = 200): APIResponse{
            return if(statusCode < 400) SuccessAPIResponse(payload, statusCode) else ErrorAPIResponse(payload, statusCode)
        }
    }

    abstract class APIResponse(val payload: Any, val statusCode: Int) {
        abstract fun serializePayload(): Any
        abstract fun serializeStatusCode(): Int
    }

    private class SuccessAPIResponse(payload: Any, statusCode: Int) : APIResponse(payload, statusCode) {

        @JsonProperty("data")
        override fun serializePayload(): Any{
            return payload
        }

        @JsonProperty("status")
        override fun serializeStatusCode(): Int{
            return statusCode
        }
    }

    private class ErrorAPIResponse(payload: Any, statusCode: Int) : APIResponse(payload, statusCode) {

        @JsonProperty("error")
        override fun serializePayload(): Any{
            return payload
        }

        @JsonProperty("status")
        override fun serializeStatusCode(): Int{
            return statusCode
        }
    }
}