package me.vivedo.forumy2.models

import com.fasterxml.jackson.annotation.JsonProperty
import me.vivedo.forumy2.dao.Users
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.sql.transactions.transaction

class User(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<User>(Users)

    var name by Users.name
    var surname by Users.surname
    var userClass by UserClass referencedOn Users.userClass

    @JsonProperty("name")
    fun serializeName(): String{
        return name
    }

    @JsonProperty("surname")
    fun serializeSurname(): String{
        return surname
    }

    @JsonProperty("class")
    fun serializeUserClass(): UserClass = transaction{
        userClass
    }
}