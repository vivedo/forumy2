package me.vivedo.forumy2.models

import me.vivedo.forumy2.dao.CourseClasses
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class CourseClass(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<CourseClass>(CourseClasses)

    val course by Course referencedOn CourseClasses.course
    val partecipantsLimit by CourseClasses.partecipantsLimit
}