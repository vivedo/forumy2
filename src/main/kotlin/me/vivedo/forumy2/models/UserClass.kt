package me.vivedo.forumy2.models

import com.fasterxml.jackson.annotation.JsonProperty
import me.vivedo.forumy2.dao.UserClasses
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class UserClass(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<UserClass>(UserClasses)

    var displayname by UserClasses.displayname

    @JsonProperty("displayname")
    fun serializeDisplayname(): String{
        return displayname
    }

    @JsonProperty("id")
    fun serializeId(): Int{
        return id.value
    }
}