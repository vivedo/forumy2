package me.vivedo.forumy2.models

import me.vivedo.forumy2.dao.Reservations
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class Reservation(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Reservation>(Reservations)

    var user by User referencedOn Reservations.user
    var courseClass by CourseClass referencedOn Reservations.courseClass
}