package me.vivedo.forumy2.models

import me.vivedo.forumy2.dao.Locations
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class Location(id: EntityID<Int>) : IntEntity(id) {
//    MI PIASCE. DIESCI.

    companion object : IntEntityClass<Location>(Locations)

    var displayname by Locations.displayname
}