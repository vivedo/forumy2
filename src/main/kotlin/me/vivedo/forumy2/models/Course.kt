package me.vivedo.forumy2.models

import me.vivedo.forumy2.dao.Courses
import org.jetbrains.exposed.dao.*

class Course(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Course>(Courses)

    var displayname by Courses.displayname
    var description by Courses.description
}